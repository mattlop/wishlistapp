# WishListApp

Une application qui gère les listes de souhaits des utilisateurs.

## Objectif

- Naviguer sur un menu `En cours...`
- Naviguer entre les listes `OK`
- Créer une liste `OK`
- Créer un "item" `OK`
- Créer une liste dans une liste `OK`
- Supprimer un "item" `OK`
- Supprimer une liste `OK`
- Un "item" et une liste possède **un titre**, **une description** et une **date de création** `OK`
- Modifier un item ou une liste

## Petite amélioration

- Filtre (*ex: trier par liste ou item*)
- Barre de recherche
- Insérer des liens
- Afficher le chemin actuel (*ex: /maListe/mesSouhaits/*)
- Afficher le nombre d'items dans une listes
- Ajouter des images

## Pour aller plus loin

- Créer un compte 
- Ajouter des amis 
- Voir la listes de ses amis
- Enregister sa liste en ligne

## Nom des classes et layouts

| Fichier | Type | Fonction |
|--------|-----------|---------|
| **Layouts** |||
| *activity_list*|XML| Afficher une liste |
| *activity_main*|XML| Le menu |
| *activity_new_wish*|XML| Interface de création d'une liste ou item |
| *wish*|XML| Afficher un item |
| *wish_add*|XML| Button de création |
| **ListActivity**|||
| *ListActivity*|Classe Kotlin|Gère l'activité des listes|
| *WishAdapter*| Classe Kotlin|Gère la construction d'une liste|
| **SQLiteData**|||
| *WishItemDB*|Classe Kotlin|Execute les requêtes pour la BDD|
| *WishItemDBHelper*|Classe Kotlin|Manage les tables de la BDD|
| *Tables*|Classe Kotlin|Référence les tables et leurs colonnes|
| *WishInterface*|Interface Kotlin|Interface pour les données|
| *WishItem*|Classe Kotlin|Donnée d'un Item|
| *WishList*|Classe Kotlin|Donnée d'une Liste|
| **NewWishActivity**|||
|*NewWishActivity*|Classe Kotlin|Créer un item ou une liste|

## Composant utilisés

* `RecyclerView`
* `AnkoSQLiteData`
* `AnkoCommon`
