package com.example.wishlistapp

import android.app.Application
import android.app.Instrumentation
import android.content.Context
import com.example.wishlistapp.SQLiteData.DB.WishItemDB
import com.example.wishlistapp.SQLiteData.DBHelper.WishItemDbHelper
import org.jetbrains.anko.internals.AnkoInternals
import org.jetbrains.anko.internals.AnkoInternals.getContext
import org.junit.Test
import androidx.test.core.app.ApplicationProvider
import androidx.test.platform.app.InstrumentationRegistry
import com.example.wishlistapp.SQLiteData.DBTables.WishItem
import org.junit.Assert.*
import org.junit.Before
import java.security.AccessController.getContext

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    private lateinit var instrContext: Context

    @Before
    fun setup(){
        instrContext = InstrumentationRegistry.getInstrumentation().context
    }

    @Test
    fun sqlite_wishitem_test(){
        val TITLE = "unTitre"
        val DESC = "uneDescription"
        val DATE = "10/10/2020"
        val WishItemDb by lazy { WishItemDB(WishItemDbHelper(instrContext)) }
        val wi : WishItem = WishItem(TITLE,DESC,null,DATE)
        var listWI = listOf<WishItem>()
        WishItemDb.saveWishItem(wi)
        listWI = WishItemDb.requestWishItem()
        assertEquals(listWI[0].title,TITLE)
        assertEquals(listWI[0].description,DESC)
        assertEquals(listWI[0].dateCreated,DATE)
    }
}