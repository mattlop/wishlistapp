package com.example.wishlistapp.SQLiteData.DBTables

data class WishList(val id : Int, val rang : Int,val title: String, val description: String, val image: String, val dateCreated: String, val parentId : Int) : WishInterface {
    override fun getIdWish(): Int = id

    override fun getNameTitle(): String = title

    override fun getDesc(): String = description

    override fun getRangList(): Int = rang

    override fun getImageSource(): String = image

    override fun getDateCreate(): String = dateCreated

    override fun getIdParent(): Int = parentId

    override fun getTypeWish(): String = "WishList"
}
/*
* parentId prend la valeur -1 quand celui-ci n'en possède pas
*/