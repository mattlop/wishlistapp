package com.example.wishlistapp.SQLiteData.DBHelper

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.wishlistapp.SQLiteData.DBTables.Tables
import org.jetbrains.anko.db.*

class WishItemDbHelper(ctx : Context) : ManagedSQLiteOpenHelper(ctx, DB_NAME,null, DB_VERSION) {
    companion object {
        val DB_NAME = "wishitem.db"
        val DB_VERSION = 3
    }
    //à la création de la base, créer les tables
    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(Tables.ItemWishTable.NAME, true, Tables.ItemWishTable.ID to INTEGER + PRIMARY_KEY + UNIQUE,
            Tables.ItemWishTable.RANG to INTEGER,
            Tables.ItemWishTable.TITLE to TEXT,
            Tables.ItemWishTable.DESCRIPTION to TEXT,
            Tables.ItemWishTable.IMAGE to TEXT,
            Tables.ItemWishTable.DATECREATED to TEXT,
            Tables.ItemWishTable.PARENTID to INTEGER
        )
        db?.createTable(Tables.ListWishTable.NAME, true, Tables.ListWishTable.ID to INTEGER + PRIMARY_KEY + UNIQUE,
            Tables.ListWishTable.RANG to INTEGER,
            Tables.ListWishTable.TITLE to TEXT,
            Tables.ListWishTable.DESCRIPTION to TEXT,
            Tables.ListWishTable.IMAGE to TEXT,
            Tables.ListWishTable.DATECREATED to TEXT,
            Tables.ListWishTable.PARENTID to INTEGER
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(Tables.ItemWishTable.NAME, true)
        db?.dropTable(Tables.ListWishTable.NAME, true)
        onCreate(db)
    }
}