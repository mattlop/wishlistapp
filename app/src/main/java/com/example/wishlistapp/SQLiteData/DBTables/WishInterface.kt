package com.example.wishlistapp.SQLiteData.DBTables

interface WishInterface {
    fun getIdWish() : Int
    fun getNameTitle() : String
    fun getDesc() : String
    fun getRangList() : Int
    fun getImageSource() : String
    fun getDateCreate() : String
    fun getIdParent() : Int

    fun getTypeWish() : String
}