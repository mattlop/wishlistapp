package com.example.wishlistapp.SQLiteData.DBTables

class Tables {

    object ItemWishTable {
        const val NAME = "ItemWish"
        const val ID = "id"
        const val RANG = "rang"
        const val TITLE = "title"
        const val DESCRIPTION = "description"
        const val IMAGE = "image"
        const val DATECREATED = "dateCreated"
        const val PARENTID = "parentId"
    }

    object  ListWishTable{
        const val NAME = "WishList"
        const val ID = "id"
        const val RANG = "rang"
        const val TITLE = "title"
        const val DESCRIPTION = "description"
        const val IMAGE = "image"
        const val DATECREATED = "dateCreated"
        const val PARENTID = "parentId"
    }
}