package com.example.wishlistapp.SQLiteData.DB

import com.example.wishlistapp.SQLiteData.DBHelper.WishItemDbHelper
import com.example.wishlistapp.SQLiteData.DBTables.Tables
import com.example.wishlistapp.SQLiteData.DBTables.WishInterface
import com.example.wishlistapp.SQLiteData.DBTables.WishItem
import com.example.wishlistapp.SQLiteData.DBTables.WishList
import org.jetbrains.anko.db.*

class WishItemDB(private val dbHelper: WishItemDbHelper) {

    /*        SELECT         */
    //La fonction select qui renvoie une liste de WishItem
    private fun requestWishItem() = dbHelper.use{
        select(Tables.ItemWishTable.NAME,
            Tables.ItemWishTable.ID,
            Tables.ItemWishTable.RANG,
            Tables.ItemWishTable.TITLE,
            Tables.ItemWishTable.DESCRIPTION,
            Tables.ItemWishTable.IMAGE,
            Tables.ItemWishTable.DATECREATED,
            Tables.ItemWishTable.PARENTID
        ).parseList(classParser<WishItem>())
    }
    //La fonction select qui renvoie une liste de WishList
    private fun requestWishList() = dbHelper.use{
        select(Tables.ListWishTable.NAME,
            Tables.ListWishTable.ID,
            Tables.ListWishTable.RANG,
            Tables.ListWishTable.TITLE,
            Tables.ListWishTable.DESCRIPTION,
            Tables.ListWishTable.IMAGE,
            Tables.ListWishTable.DATECREATED,
            Tables.ListWishTable.PARENTID
        ).parseList(classParser<WishList>())
    }

    //Récupère les WishItem selon l'id du parent (-1 sans parent)
    private fun selectWishItem(idParent : Int) = dbHelper.use{
        select(Tables.ItemWishTable.NAME,
            Tables.ItemWishTable.ID,
            Tables.ItemWishTable.RANG,
            Tables.ItemWishTable.TITLE,
            Tables.ItemWishTable.DESCRIPTION,
            Tables.ItemWishTable.IMAGE,
            Tables.ItemWishTable.DATECREATED,
            Tables.ItemWishTable.PARENTID
        ).whereArgs(
            "${Tables.ItemWishTable.PARENTID} = $idParent"
        ).orderBy(
            "${Tables.ItemWishTable.RANG}" , SqlOrderDirection.ASC
        ).parseList(classParser<WishItem>())
    }

    //Récupère les WishItem selon l'id du parent (-1 sans parent)
    private fun selectWishList(idParent : Int) = dbHelper.use{
        select(Tables.ListWishTable.NAME,
            Tables.ListWishTable.ID,
            Tables.ListWishTable.RANG,
            Tables.ListWishTable.TITLE,
            Tables.ListWishTable.DESCRIPTION,
            Tables.ListWishTable.IMAGE,
            Tables.ListWishTable.DATECREATED,
            Tables.ListWishTable.PARENTID
        ).whereArgs(
            "${Tables.ListWishTable.PARENTID} = $idParent"
        ).orderBy(
            "${Tables.ListWishTable.RANG}" , SqlOrderDirection.ASC
        ).parseList(classParser<WishList>())
    }

    //Selectionne uniquement les objets sans parents donc à la racine
    fun selectWishRoot() : List<WishInterface> = selectWishByIdParent(-1)

    //Selectionne les objets selon l'id de sont parents
    fun selectWishByIdParent(parentId : Int) : List<WishInterface>{
        val wishItemRootList = selectWishItem(parentId)
        val wishListRootList = selectWishList(parentId)
        val listWish = mutableListOf<WishInterface>()
        listWish.addAll(wishListRootList)
        listWish.addAll(wishItemRootList)
        return listWish.toList()
    }

    /*        INSERT         */
    //La fonction insert qui demande un object WishItem
    fun saveWishItem(wishItem: WishItem) = dbHelper.use{
        insert(Tables.ItemWishTable.NAME,
                Tables.ItemWishTable.RANG to wishItem.rang,
                Tables.ItemWishTable.TITLE to wishItem.title,
                Tables.ItemWishTable.DESCRIPTION to wishItem.description,
                Tables.ItemWishTable.DATECREATED to wishItem.dateCreated,
                Tables.ItemWishTable.IMAGE to wishItem.image,
                Tables.ItemWishTable.PARENTID to wishItem.parentId
        )
    }
    //La fonction insert qui demande un object WishList
    fun saveWishList(wishlist: WishList) = dbHelper.use{
        insert(Tables.ListWishTable.NAME,
            Tables.ListWishTable.RANG to wishlist.rang,
            Tables.ListWishTable.TITLE to wishlist.title,
            Tables.ListWishTable.DESCRIPTION to wishlist.description,
            Tables.ListWishTable.DATECREATED to wishlist.dateCreated,
            Tables.ListWishTable.IMAGE to wishlist.image,
            Tables.ListWishTable.PARENTID to wishlist.parentId
        )
    }
    //Insère une liste d'objet WishItem dans la BDD
    fun saveWishItems(wishList: List<WishItem>){
        for (wi in wishList){
            saveWishItem(wi)
        }
    }
    //Insère une liste d'objet WishList dans la BDD
    fun saveWishLists(wishList: List<WishList>){
        for (wl in wishList){
            saveWishList(wl)
        }
    }

    /*           DROP              */

    fun deleteWishItem(idItem : Int) = dbHelper.use {
        delete(Tables.ItemWishTable.NAME,"${Tables.ItemWishTable.ID} = $idItem")
    }

    fun deleteWishList(idItem: Int) = dbHelper.use {
        delete(Tables.ListWishTable.NAME,"${Tables.ItemWishTable.ID} = $idItem")
    }
}