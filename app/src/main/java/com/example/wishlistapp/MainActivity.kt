package com.example.wishlistapp

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.wishlistapp.ListActivity.ListActivity
import com.example.wishlistapp.MenuActivity.MenuActivity


class MainActivity : AppCompatActivity() {

    private lateinit var buttonActivity: Button;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val MenuActivity = Intent(this@MainActivity, MenuActivity::class.java)
        startActivity(MenuActivity)
        buttonActivity = findViewById(R.id.button2);
        buttonActivity.setOnClickListener {
            val ListActivity = Intent(this@MainActivity, ListActivity::class.java)
            ListActivity.putExtra("parentId", -1)
            startActivity(ListActivity)
        }
    }

}