package com.example.wishlistapp.NewWishActivity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.wishlistapp.ListActivity.ListActivity
import com.example.wishlistapp.R
import com.example.wishlistapp.SQLiteData.DB.WishItemDB
import com.example.wishlistapp.SQLiteData.DBHelper.WishItemDbHelper
import com.example.wishlistapp.SQLiteData.DBTables.WishItem
import com.example.wishlistapp.SQLiteData.DBTables.WishList
import kotlinx.android.synthetic.main.activity_new_wish.*
import org.jetbrains.anko.toast
import java.time.LocalDate

class NewWishActivity : AppCompatActivity() {

    val wishDb by lazy { WishItemDB(WishItemDbHelper(applicationContext)) }
    var parentId : Int = -1
    var rang : Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_wish)

        val b = intent.extras
        val parentIdFound = b!!.getInt("parentId")
        val rangFound = b!!.getInt("rang")

        parentId = parentIdFound
        rang = rangFound
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun CreateWish(sender : View){
        val title = editTextTitle.text.toString()
        val desc = editTextDescription.text.toString()
        val idRadios = rg_type.checkedRadioButtonId
        if(rb_item.id == idRadios) {
            val wishItem = WishItem(-1,rang,title,desc,R.drawable.ic_launcher_foreground.toString(),LocalDate.now().toString(),parentId)
            wishDb.saveWishItem(wishItem)
        }else{
            val wishList = WishList(-1,rang,title,desc,R.drawable.ic_launcher_foreground.toString(),LocalDate.now().toString(),parentId)
            wishDb.saveWishList(wishList)
        }

        finish()
    }

}