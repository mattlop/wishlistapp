package com.example.wishlistapp.ListActivity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.children
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wishlistapp.NewWishActivity.NewWishActivity
import com.example.wishlistapp.R
import com.example.wishlistapp.R.layout.activity_list
import com.example.wishlistapp.SQLiteData.DB.WishItemDB
import com.example.wishlistapp.SQLiteData.DBHelper.WishItemDbHelper
import com.example.wishlistapp.SQLiteData.DBTables.WishInterface
import com.example.wishlistapp.SQLiteData.DBTables.WishItem
import kotlinx.android.synthetic.main.activity_list.*
import kotlinx.android.synthetic.main.wish_add.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton
import java.time.LocalDate


class ListActivity : AppCompatActivity() {

    val wishDb by lazy { WishItemDB(WishItemDbHelper(applicationContext)) }

    var parentId = -1

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_list)
        wishRecyclerView.layoutManager = LinearLayoutManager(this)

        val b = intent.extras
        val parentId = b!!.getInt("parentId")
        this.parentId = parentId

        RefreshAdapter()
    }

    override fun onResume() {
        super.onResume()
        RefreshAdapter()
    }

    fun GetSqlitedata(parentId : Int) : Array<WishInterface> {
        return wishDb.selectWishByIdParent(parentId).toTypedArray();
    }

    //Fonction de test d'entrer de valeur
    @RequiresApi(Build.VERSION_CODES.O)
    fun TestAddValue(){
        val item = WishItem(
            5,
            0,
            "myTitle",
            "myDesc",
            R.drawable.lol.toString(),
            LocalDate.now().toString(),
            -1
        )
        wishDb.saveWishItem(item)
    }

    fun OpenNewList(idList : Int){
        val listActivity = Intent(this,ListActivity::class.java)
        listActivity.putExtra("parentId",idList)
        startActivity(listActivity)
    }

    fun CreateWish(sender : View){
        val newWishActivity = Intent(this, NewWishActivity::class.java)
        newWishActivity.putExtra("parentId", parentId)
        newWishActivity.putExtra("rang", wishRecyclerView.childCount)
        startActivity(newWishActivity)
    }

    fun DeleteWishItem(idWishItem : Int){
        alert("Voulez vous vraiment le supprimer l'objet ?") {
            yesButton {
                wishDb.deleteWishItem(idWishItem)
                RefreshAdapter()
            }
            noButton { }
        }.show()
    }

    fun DeleteWishList(idWishList : Int){
        alert("Voulez vous vraiment le supprimer la liste ?") {
            yesButton {
                wishDb.deleteWishList(idWishList)
                RefreshAdapter()
            }
            noButton { }
        }.show()
    }

    fun RefreshAdapter(){
        wishRecyclerView.adapter = wishAdapter(GetSqlitedata(parentId),this@ListActivity)
    }
}