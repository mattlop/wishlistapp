package com.example.wishlistapp.ListActivity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.example.wishlistapp.R
import com.example.wishlistapp.SQLiteData.DBTables.WishInterface
import com.example.wishlistapp.SQLiteData.DBTables.WishItem
import com.example.wishlistapp.SQLiteData.DBTables.WishList
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.wish.view.*

class wishAdapter(var wishList: Array<WishInterface>, val parentActicity : ListActivity) :RecyclerView.Adapter<wishAdapter.ViewHolder>() {

    class ViewHolder(view: View, val parentActicity: ListActivity) : RecyclerView.ViewHolder(view), LayoutContainer {
        override val containerView: View?
            get() = itemView
            fun bindWish(Wish: WishInterface) {
                with(Wish) {
                    itemView.wishTxt.text = getNameTitle()
                    itemView.wishDesc.text = getDesc()
                    itemView.wishImg.setImageResource(getImageSource().toInt())
                    if(this is WishItem){
                        itemView.buttonDelete.setOnClickListener { parentActicity.DeleteWishItem(getIdWish()) }
                    }else if(this is WishList){
                        itemView.viewline.visibility = View.INVISIBLE
                        itemView.background = parentActicity.getDrawable(R.color.purple_200)
                        itemView.setOnClickListener { parentActicity.OpenNewList(getIdWish()) }
                        itemView.buttonDelete.setOnClickListener { parentActicity.DeleteWishList(getIdWish()) }
                    }
                }
            }

        }
        override fun getItemCount(): Int = wishList.size

    fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
        return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.wish),parentActicity)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindWish(wishList[position])
    }
}